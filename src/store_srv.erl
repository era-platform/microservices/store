%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 12.08.2021
%%% @doc

-module(store_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-export([wait_mnesia/1, load_table/1, filter_expired/1, ready/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-record(state, {
    regname :: binary(),
    ref :: reference(),
    tablename :: atom(),
    ets :: ets:tab(),
    is_ready :: boolean()
}).

-record(item, {
    key,
    value,
    ttl,
    timestamp,
    ref
}).

-define(TableName, store).

%% ====================================================================
%% Public functions
%% ====================================================================

start_link(Opts) when is_map(Opts) ->
    RegName = regname(),
    ?GLOBAL:gen_server_start_link({global, RegName}, ?MODULE, Opts#{'name' => RegName}, []).

%% @private
regname() ->
    ?GN_NAMING:get_globalname(?MsvcType).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    Self = self(),
    Ref = make_ref(),
    State = #state{regname = ?BU:get_by_key('name',Opts),
                   ref = Ref,
                   tablename = ?TableName,
                   ets = ets:new(timers,[public,set]),
                   is_ready = false},
    % ----
    Self ! {'init',Ref},
    % ----
    ?LOG('$info', "~ts. srv inited", [?APP]),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

%% Debug restart
handle_call({restart}, _From, State) ->
    {stop,State};

%% --------------
%% get
handle_call({get,_}, _From, #state{is_ready=false}=State) -> {reply,{error,<<"Storage is not ready">>},State};
handle_call({get,[Key]}, From, #state{tablename=TableName}=State) ->
    workf(Key, fun() -> gen_server:reply(From,do_get([Key],TableName)) end),
    {noreply,State};

%% --------------
%% get
handle_call({get_ttl,_}, _From, #state{is_ready=false}=State) -> {reply,{error,<<"Storage is not ready">>},State};
handle_call({get_ttl,[Key]}, From, #state{tablename=TableName}=State) ->
    workf(Key, fun() -> gen_server:reply(From,do_get_ttl([Key],TableName)) end),
    {noreply,State};

%% --------------
%% put
handle_call({put,_}, _From, #state{is_ready=false}=State) -> {reply,{error,<<"Storage is not ready">>},State};
handle_call({put,[Key,Value,TTLms]}, From, #state{tablename=TableName,ets=TimersEts}=State) ->
    Self = self(),
    workf(Key, fun() -> gen_server:reply(From, do_put([Key,Value,TTLms],{Self,TableName,TimersEts})) end),
    {noreply,State};

%% --------------
%% delete
handle_call({delete,_}, _From, #state{is_ready=false}=State) -> {reply,{error,<<"Storage is not ready">>},State};
handle_call({delete,[Key]}, From, #state{tablename=TableName,ets=TimersEts}=State) ->
    workf(Key, fun() -> gen_server:reply(From, do_delete([Key],{TableName,TimersEts})) end),
    {noreply,State};

%% --------------
%% Other
handle_call(_Request, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

%% --------------
%% put
handle_cast({put,_}, #state{is_ready=false}=State) -> {noreply,State};
handle_cast({put,[Key,Value,TTLms]}, #state{tablename=TableName,ets=TimersEts}=State) ->
    Self = self(),
    workf(Key, fun() -> do_put([Key,Value,TTLms],{Self,TableName,TimersEts}) end),
    {noreply,State};

%% --------------
%% delete
handle_cast({delete,_}, #state{is_ready=false}=State) -> {noreply,State};
handle_cast({delete,[Key]}, #state{tablename=TableName,ets=TimersEts}=State) ->
    workf(Key, fun() -> do_delete([Key],{TableName,TimersEts}) end),
    {noreply,State};

%% -------------
%% Other
handle_cast(_Request, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% --------------
%% init
handle_info({init,Ref}, #state{ref=Ref}=State) ->
    State1 = init_data(State),
    {noreply, State1};

%% --------------
%% expired
handle_info({expired,Key,Ref}, #state{tablename=TableName,ets=TimerEts}=State) ->
    F = fun() ->
            case mnesia_get(TableName,Key) of
                {ok, #item{ref=Ref}} ->
                    ets:delete(TimerEts,Key),
                    mnesia_delete(TableName,Key);
                _ -> ok
            end end,
    workf(Key, F),
    {noreply, State};

%% --------------
%% other
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% Loading
%% ====================================================================

init_data(#state{}=State) ->
    {_,State1} = lists:foldl(fun(FName, {ok,StateAcc}) -> ?MODULE:FName(StateAcc);
                                (_,Acc) -> Acc
                             end, {ok,State}, [wait_mnesia, load_table, filter_expired, ready]),
    State1.

%% @private
wait_mnesia(State) ->
    case ?MNESIALIB:get_schema_status() of
        {ok,'running'} ->
            {ok,State};
        _ ->
            Ref1 = make_ref(),
            erlang:send_after(100,self(),{init,Ref1}),
            {retry,State#state{ref=Ref1}}
    end.

%% @private
load_table(#state{tablename=TableName}=State) ->
    Opts = #{'app_name' => ?APP,
             'app_version' => undefined,
             'load_timeout' => 20000,
             'update_function' => fun({_AppName,_AppVersion},Rec0) -> Rec0 end,
             'index_fields' => []},
    case ?MNESIALIB:ensure_table(TableName, item, record_info(fields,item), Opts) of
        ok ->
            {ok, State};
        {error,Reason} ->
            ?LOG('$error',"~ts. mnesia load table error: ~120tp...",[?APP,Reason]),
            Ref1 = make_ref(),
            erlang:send_after(100,self(),{init,Ref1}),
            {retry,State#state{ref=Ref1}};
        {retry_after,Timeout,Reason} ->
            ?LOG('$info',"~ts. wait for mnesia to ensure and load table... (~ts)",[?APP,Reason]),
            Ref1 = make_ref(),
            Timeout1 = case Timeout of X when X > 1000 -> 1000; X when X < 100 -> 100; X -> X end,
            erlang:send_after(Timeout1,self(),{init,Ref1}),
            {retry,State#state{ref=Ref1}}
    end.

%% @private
filter_expired(#state{tablename=TableName,ets=Ets}=State) ->
    NowTS = ?BU:timestamp(),
    Self = self(),
    ets:foldl(fun(#item{key=Key,timestamp=TS,ttl=TTL},_) when TS + TTL =< NowTS -> mnesia_delete(TableName, Key);
                 (#item{key=Key,timestamp=TS,ttl=TTL,ref=Ref},_) ->
                     TimerRef = erlang:send_after(TS + TTL - NowTS + 20, Self, {expired,Key,Ref}),
                     ets:insert(Ets,{Key,TimerRef})
              end, ok, TableName),
    {ok, State}.

%% @private
ready(State) ->
    {ok, State#state{is_ready=true}}.

%% ====================================================================
%% Internal functions
%% Async by workers
%% ====================================================================

workf(Key,Fun) ->
    F = fun() ->
            try Fun()
            catch E:R:ST -> ?LOG('$crash',"storage operation crashed (key=~120tp): {~tp, ~tp}~n\tStack: ~120tp",[Key,E,R,ST])
            end end,
    ?BLwrk:cast_workf(Key,F).

%% ====================================================================
%% Internal functions
%% Get/Put/Delete operations
%% ====================================================================

%%
do_put([Key,_,TTLms], StateX) when TTLms=<0 -> do_delete([Key],StateX);
do_put([Key,Value,TTLms], {FacadePid,TableName,TimersEts}) ->
    Ref = make_ref(),
    case ets:lookup(TimersEts,Key) of
        [{Key,TimerRef}] -> ?BU:cancel_timer(TimerRef);
        [] -> ok
    end,
    Item = #item{key=Key,value=Value,timestamp=?BU:timestamp(),ttl=TTLms,ref=Ref},
    mnesia_put(TableName,Item),
    TimerRef1 = erlang:send_after(TTLms+20,FacadePid,{expired,Key,Ref}),
    ets:insert(TimersEts,{Key,TimerRef1}),
    ok.

%%
do_delete([Key], {TableName,TimersEts}) ->
    case ets:lookup(TimersEts,Key) of
        [] -> {error,not_found};
        [{Key,TimerRef}] ->
            ?BU:cancel_timer(TimerRef),
            mnesia_delete(TableName,Key),
            ok
    end.

%%
do_get([Key], TableName) ->
    case mnesia_get(TableName,Key) of
        false -> false;
        {ok,#item{value=Value}} -> {Key,Value}
    end.

%%
do_get_ttl([Key], TableName) ->
    case mnesia_get(TableName,Key) of
        false -> false;
        {ok,#item{value=Value,timestamp=TS,ttl=TTL}} ->
            NowTS = ?BU:timestamp(),
            case TS + TTL > NowTS of
                true -> {Key,Value,NowTS-TS-TTL};
                false -> false
            end end.

%% ====================================================================
%% Internal functions
%% Mnesia operations
%% ====================================================================

%%
mnesia_delete(TableName, Key) ->
    {atomic,ok} = mnesia:transaction(fun() -> mnesia:delete({TableName,Key}) end).

%%
mnesia_put(TableName, Item) ->
    {atomic,ok} = mnesia:transaction(fun() -> mnesia:write(TableName,Item,write) end).

%%
mnesia_get(TableName, Key) ->
    case ets:lookup(TableName,Key) of
        [] -> false;
        [#item{}=Item] -> {ok,Item}
    end.