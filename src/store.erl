%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.08.2021
%%% @doc Common key-value auto expired storage microservice app.
%%%   Application could be setup by application:set_env:
%%%      log_destination
%%%          descriptor to define log file folder and prefix.
%%%          Default: {'store','erl'}

-module(store).
-author('Peter <tbotc@yandex.ru>').

-behaviour(application).

-export([start/0]).

-export([start/2,
         stop/1]).

-export([start_script/2]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public
%% ====================================================================

%% -------------------------------------------
%% Start method
%% -------------------------------------------
-spec start() -> ok | {error,Reason::term()} | {retry_after,Timeout::non_neg_integer(),Reason::term()}.
%% -------------------------------------------
start() ->
    setup_dependencies(),
    ensure_deps_started(),
    case ensure_mnesialib_started() of
        ok ->
            case application:ensure_all_started(?APP, permanent) of
                {ok, _Started} -> ok;
                Error -> Error
            end;
        {retry_after,_,_}=Retry -> Retry
    end.

%% ====================================================================
%% API functions
%% ====================================================================

start_script(_Domain,_Args) -> false.

%% ===================================================================
%% Callback functions (application)
%% ===================================================================

%% -------------------------------------
%% @doc Starts application
%% -------------------------------------
start(_, _) ->
    ?OUT('$info',"~ts. ~p start", [?APP, self()]),
    setup_dependencies(),
    ensure_deps_started(),
    ?LeaderSupv:start_link(?LeaderCallback,?AppLeaderName).

%% -------------------------------------
%% @doc Stops application.
%% -------------------------------------
stop(State) ->
    ?OUT('$info',"~ts: Application stopped (~120p)", [?APP, State]),
    ok.

%% ====================================================================
%% Internal
%% ====================================================================

%% @private
setup_dependencies() ->
    % mnesialib
    SchemaNodes = ?LeaderCallback:get_app_nodes(),
    ?BU:set_env(?MNESIALIB, 'log_destination', {store,?MNESIALIB}),
    ?BU:set_env(?MNESIALIB, 'schema_nodes', SchemaNodes),
    ?BU:set_env(?MNESIALIB, 'schema_synchronized_function', fun() -> ok end),
    ok.

%% --------------------------------------
%% @private
%% starts deps applications, that out of app link
%% --------------------------------------
ensure_deps_started() ->
    {ok,_} = application:ensure_all_started(?PLATFORMLIB, permanent),
    {ok,_} = application:ensure_all_started(?MNESIALIB, permanent).

%% --------------------------------------
%% @private
%% check mnesialib started and synchronized
%% --------------------------------------
ensure_mnesialib_started() ->
    FunWait = case lists:keyfind(?MNESIALIB,1,application:which_applications()) of
                  {_,_,_} -> fun() -> ok end;
                  false -> fun() -> F = fun F(I) when I =< 0 -> ok;
                      F(I) ->
                          case ?MNESIALIB:get_schema_status() of
                              {ok,'running'} -> 0;
                              _ -> timer:sleep(100), F(I-1)
                          end end, F(10) end end,
    application:ensure_all_started(?BASICLIB),
    application:ensure_all_started(?MNESIALIB),
    FunWait(),
    case ?MNESIALIB:get_schema_status() of
        {ok,'running'} -> ok;
        _ -> {retry_after, 5000, "wait for mnesia to sync schema"}
    end.